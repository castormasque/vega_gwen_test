import json 

def change_nodes(data_nodes, node_mapping, group_mapping):
    return [{
        'name':d['name'],
        'index':node_mapping[d['index']],
        'group':group_mapping[d['group']]
    } for d in data_nodes]

def change_links(data_links, node_mapping):
    return [{
        'source':node_mapping[d['source']],
        'target':node_mapping[d['target']],
        'value':d['value']
    } for d in data_links]

if __name__ == "__main__":

    # _in = '/home/regis/work/vega_gwen_test/public/data/net_24nodes.json'
    # _out = '/home/regis/work/vega_gwen_test/public/data/net_24nodes_v2.json'
    _in = '/home/regis/work/vega_gwen_test/public/data/net_151nodes.json'
    _out = '/home/regis/work/vega_gwen_test/public/data/net_151nodes_v2.json'

    data = None
    with open(_in) as f:
        data = json.load(f)

    # get unique list of indices
    indices = {i['index'] for i in data['nodes']}
    # build a mapping: unique index -> unique index from 0 to n
    node_mapping = {y:x for x,y in enumerate(sorted(indices))}
    # get unique list of groups
    groups = {i['group'] for i in data['nodes']}
    # build a mapping: unique group id -> unique group id from 1 to n
    group_mapping = {y:x+1 for x,y in enumerate(sorted(groups))}

    data['nodes'] = change_nodes(data['nodes'], node_mapping, group_mapping)
    data['links'] = change_links(data['links'], node_mapping)


    with open(_out, mode='w') as f:
        json.dump(data, f)

