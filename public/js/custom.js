// when dom is ready
(function() {

    var view;

    function createButton(_text, context, func){
        var button = document.createElement("input");
        button.type = "button";
        button.value = _text;
        button.onclick = func;
        console.log(context);
        console.log(_text);
        context.appendChild(button);
    }
    
    function render(spec) {
        view = new vega.View(vega.parse(spec))
            .renderer('canvas')  // set renderer (canvas or svg)
            .initialize('#view') // initialize view within parent DOM container
            .hover()             // enable hover encode set processing
            .run();
    
        document.getElementsByTagName('canvas')[0].style.border = '1px solid red';
    }
    

    //----------------------------------------
    // EXECUTE


    function load_dataset(name){
        vega.loader()
            .load(name)
            .then(function(data) { 
                render(JSON.parse(data));
            });
    } 
    

    window.onload = function(){
        createButton('24 nodes', document.body, function(){
            load_dataset('../layouts/force_layout_24nodes.json');
        });
        createButton('151 nodes', document.body, function(){
            load_dataset('../layouts/force_layout_151nodes.json');
        });
    }

    // load first
    load_dataset('../layouts/force_layout_24nodes.json');

 })();

